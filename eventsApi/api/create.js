'use strict';

const uuid = require('uuid');
const dynamodb = require('../lib/dynamodb');
const config = require('../config.json');

module.exports.post = (event, context, callback) => {
    const data = JSON.parse(event.body);

    if (
        typeof data.eventName == 'string' &&
        typeof data.startDate == 'number' &&
        typeof data.endDate == 'number'
    ) {
        const timestamp = new Date().getTime();
        const params = {
            TableName: config.eventsTableName,
            Item: {
                id: uuid.v1(),
                createdAt: timestamp
            }
        };
        params.Item = Object.assign(params.Item, data)
        // write the event to the database
        dynamodb.put(params, (error) => {
            // handle potential errors
            if (error) {
                console.error(error);
                callback(new Error('Couldn\'t create the event item.'));
                return;
            }

            // create a response
            const response = {
                statusCode: 200,
                body: JSON.stringify(params.Item),
            };
            callback(null, response);
        });

    } else {
        console.error('Validation Failed');
        callback(new Error('Couldn\'t create event item. Data validation failed.'));
        return;
    }
};