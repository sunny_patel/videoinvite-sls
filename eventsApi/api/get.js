'use strict';

const uuid = require('uuid');
const dynamodb = require('../lib/dynamodb');
const config = require('../config.json');

module.exports.get = (event, context, callback) => {

    const timestamp = new Date().getTime();
    const params = {
        TableName: config.eventsTableName
    };

    // write the event to the database
    dynamodb.scan(params, (error, data) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(new Error('Couldn\'t scan '+ config.eventsTableName +' table.'));
            return;
        }

        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(data),
        };
        callback(null, response);
    });
};