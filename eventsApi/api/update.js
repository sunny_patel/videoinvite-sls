'use strict';

const dynamodb = require('../lib/dynamodb');
const config = require('../config.json');

module.exports.put = (event, context, callback) => {
    const data = JSON.parse(event.body);

    if (
        typeof data.id == 'string'
    ) {
        const timestamp = new Date().getTime();
        const params = {
            TableName: config.eventsTableName,
            Item: {
                id: event.pathParameters.id,
                updatedAt: timestamp
            }
        };

        params.Item = Object.assign(params.Item, data)

        // write the event to the database
        dynamodb.put(params, (error) => {
            // handle potential errors
            if (error) {
                console.error(error);
                callback(new Error('Couldn\'t update the event.'));
                return;
            }

            // create a response
            const response = {
                statusCode: 200,
                body: JSON.stringify(params.Item),
            };
            callback(null, response);
        });

    } else {
        console.error('Validation Failed');
        callback(new Error('Couldn\'t create event item. Data validation failed.'));
        return;
    }
};