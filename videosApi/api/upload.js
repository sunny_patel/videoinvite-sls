'use strict';

const config = require('../config.json');
const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

const s3 = new AWS.S3();

module.exports.post = (event, context, callback) => {
    const eventId = event.pathParameters.eventId;
    if (eventId) {
        const params = {
            Bucket: config.eventsVideoBucketName,
            Key: eventId + "/original.mp4",
            Body: "Testing test 123"
        };

        s3.putObject(params, (err, data) => {
            if (err) {
                // an error occurred
                console.log(err, err.stack);
            } else {
                // successful response
                const resp = {
                    statusCode: 200,
                    body: JSON.stringify(data)
                };

                callback(null, resp);
            }
        });
    } else {
        // No event id passed
        const resp = {
            statusCode: 500,
            body: "EventId missing."
        };
        callback(null, resp);
    }

};